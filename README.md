# Authentication Login and Signup - React App
A simple React App that allows users to create an account and login using the set credentials posted in the form. This application explores the concept of frontend authentication using tokens through React and the use of local storage in the browser. Some actions and visuals are not visible without proper authentication such that a user is logged in or not.

## Setup

Backend

```
npm install
npm run start
```

Frontend

```
npm install
npm run start
```
